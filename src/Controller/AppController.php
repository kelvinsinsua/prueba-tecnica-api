<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Constraints\Type as NumberConstraint;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use App\Entity\Client;
use App\Entity\Wallet;
use App\Entity\Transaction;
use App\Entity\Purchase;

/**
 * Description of AppController
 *
 * @author Kelvins Insua
 */
class AppController extends Controller
{

    /**
     * @Route("/api/register", name="api_register", methods="POST")
     */
    public function registroClientes(Request $request)
    {
        
        $email = $request->get('email');
        $fullName = $request->get('fullName');
        $document = $request->get('document');
        $phone = $request->get('phone');
        $errors = [];

        if (!$email) {
            $errors[] = ['email' => 'El campo correo electrónico es requerido'];
        }else{
            $emailConstraint = new EmailConstraint();
            $validationErrors = $this->get('validator')->validate(
                    $email, $emailConstraint
            );
            if (count($validationErrors) > 0) {
                $errors[] = ['email' => 'Ingrese un correo electrónico válido'];
            }

            
        }

        if (!$document) {
            $errors[] = ['document' => 'El campo documento es requerido'];
        }
        if (!$fullName) {
            $errors[] = ['fullName' => 'El campo nombres es requerido'];
        }
        if (!$phone) {
            $errors[] = ['phone' => 'Ingrese un numero de teléfono válido'];
        }
        
        $client = $this->getDoctrine()->getManager()->getRepository(Client::class)->findOneBy(['email' => $email]);
        if($client instanceof Client){
            $errors[] = ['email' => 'El correo electrónico ya esta en uso.'];
        }
        $documento = $this->getDoctrine()->getManager()->getRepository(Client::class)->findOneBy(['document' => $document]);
        if($client instanceof Client){
            $errors[] = ['document' => 'Ya existe un usuario con el mismo documento.'];
        }

        
        if(count($errors) > 0){
           return  $this->response(400,'Petición inválida.',$errors);
        }
        
        $client = new Client();
        $client->setFullName($fullName);
        $client->setEmail($email);
        $client->setPhone($phone);
        $client->setDocument($document);

        $this->getDoctrine()->getManager()->persist($client);

        $wallet = new Wallet();
        $wallet->setBalance(0);
        $wallet->setClient($client);

        $this->getDoctrine()->getManager()->persist($wallet);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->response(200, 'Regístro exitoso.');

    }

    /**
     * @Route("/api/add/balance", name="api_add_balance", methods="POST")
     */
    public function recargaBilletera(Request $request)
    {
        $document = $request->get('document');
        $phone = $request->get('phone');
        $amount = floatval($request->get('amount'));
        $errors = [];

        if (!$document) {
            $errors[] = ['document' => 'El campo documento es requerido'];
        }
        if (!$phone) {
            $errors[] = ['phone' => 'El campo celular es requerido'];
        }
        if (!$amount || !is_float($amount)) {
            $errors[] = ['amount' => 'Ingrese un monto válido'];
        }

        $client = $this->getDoctrine()->getManager()->getRepository(Client::class)->findOneBy([
            'document' => $document,
            'phone' => $phone
        ]);

        if(!$client instanceof Client){
            $errors[] = ['client' => 'Documento o número de teléfono inválido'];
        }

        

        if(count($errors) > 0){
            return  $this->response(400,'Petición inválida.',$errors);
        }

        $wallet = $client->getWallet();

        $transaction = new Transaction();
        $wallet->setBalance($wallet->getBalance() + $amount);
        $transaction->setAmount($amount);
        $transaction->setType(1);
        $transaction->setBalance($wallet->getBalance());
        $transaction->setWallet($wallet);

        $this->getDoctrine()->getManager()->persist($transaction);
        $this->getDoctrine()->getManager()->flush();

        return $this->response(200, 'Recarga exitosa.');
    }

    /**
     * @Route("/api/purchase", name="api_purchase", methods="POST")
     */
    public function crearCompra(Request $request, MailerInterface $mailer)
    {
        $document = $request->get('document');
        $phone = $request->get('phone');
        $amount = floatval($request->get('amount'));
        $errors = [];

        if (!$document) {
            $errors[] = ['document' => 'El campo documento es requerido'];
        }
        if (!$phone) {
            $errors[] = ['phone' => 'El campo celular es requerido'];
        }
        if (!$amount || !is_float($amount)) {
            $errors[] = ['amount' => 'Ingrese un monto válido'];
        }

        $client = $this->getDoctrine()->getManager()->getRepository(Client::class)->findOneBy([
            'document' => $document,
            'phone' => $phone
        ]);

        if(!$client instanceof Client){
            $errors[] = ['client' => 'Documento o número de teléfono inválido'];
        }

        

        $sessionId = md5(uniqid(rand(), true));
        $token = '';
            $pattern = '1234567890';
            $max = strlen($pattern) - 1;
            for ($i = 0; $i < 6; $i++)
                $token .= $pattern{mt_rand(0, $max)};

        $purchase = new Purchase();

        try{
            $purchase->setAmount(0+$amount);
        }catch(\Exception $e){
            $errors[] = ['amount' => 'Ingrese un monto válido'];
        }
        

        if(count($errors) > 0){
            return  $this->response(400,'Petición inválida.',$errors);
        }

        $purchase->setSessionId($sessionId);
        $purchase->setToken($token);
        $purchase->setClient($client);


        $message = (new TemplatedEmail())
                    ->subject('Confirmar compra')
                    ->from('info@example.com')
                    ->to($client->getEmail())
                    ->htmlTemplate('purchase.html.twig')
                    ->context([
                        'purchase' => $purchase,
                        'url' => 'http://localhost:3000/confirm/' . $sessionId
                    ]);
                    
        $mailer->send($message);
        
        $this->getDoctrine()->getManager()->persist($purchase);
        $this->getDoctrine()->getManager()->flush();

        return $this->response(200, 'Se ha enviado un correo electrónico con la información para confirmar su compra.');


    }

    /**
     * @Route("/api/payment", name="api_payment", methods="POST")
     */
    public function pago(Request $request)
    {
        $sessionId = $request->get('sessionId');
        $token = $request->get('token');
        $errors = [];

        if (!$token) {
            $errors[] = ['token' => 'El campo token es requerido'];
        }

        $purchase = $this->getDoctrine()->getManager()->getRepository(Purchase::class)->findOneBy([
            'sessionId' => $sessionId,
            'token' => $token
        ]);

        if(!$purchase instanceof Purchase){
            $errors[] = ['purchase' => 'La compra no se ha encontrado.'];
        }else{
            if($purchase->getStatus()){
            $errors[] = ['purchase' => 'La compra ya ha sido ejecutada.'];
            }
            $wallet = $purchase->getClient()->getWallet();
            if($wallet->getBalance() < $purchase->getAmount()){
                $errors[] = ['purchase' => 'Fondos insuficientes.'];
            }
        }

        if(count($errors) > 0){
            return  $this->response(400,'Petición inválida.',$errors);
        }

        $purchase->setStatus(1);
        $wallet->setBalance($wallet->getBalance() - $purchase->getAmount());

        $transaction = new Transaction();
        $transaction->setAmount($purchase->getAmount());
        $transaction->setType(0);
        $transaction->setBalance($wallet->getBalance());
        $transaction->setWallet($wallet);

        $this->getDoctrine()->getManager()->persist($transaction);
        $this->getDoctrine()->getManager()->flush();

        return  $this->response(200,'Transacción realizada exitosamente.');
        
    }

    /**
     * @Route("/api/balance", name="api_balance", methods="GET")
     */
    public function consultaSaldo(Request $request)
    {
        $document = $request->get('document');
        $phone = $request->get('phone');
        $errors = [];

        if (!$document) {
            $errors[] = ['document' => 'El campo documento es requerido'];
        }
        if (!$phone) {
            $errors[] = ['phone' => 'El campo celular es requerido'];
        }
        

        $client = $this->getDoctrine()->getManager()->getRepository(Client::class)->findOneBy([
            'document' => $document,
            'phone' => $phone
        ]);

        if(!$client instanceof Client){
            $errors[] = ['client' => 'Documento o número de teléfono inválido'];
        }

        if(count($errors) > 0){
            return  $this->response(400,'Petición inválida.',$errors);
        }

        $balance = $client->getWallet()->getBalance();

        return  $this->response(200,'Petición exitosa.',[], [
            'balance' => $balance
        ]);

    
    }


    public function response($code = 200, $message = '', $errors = [], $payload = null){
        return new JsonResponse([
            'code' => $code,
            'message' => $message,
            'errors' => $errors,
            'payload' => $payload
        ]);
    }

}